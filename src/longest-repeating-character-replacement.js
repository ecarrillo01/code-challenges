//https://leetcode.com/problems/longest-repeating-character-replacement/


function charactersReplacement(str, k) {
    const arr = str.split("");
    const count = {};
    let res = 0;
    const l = 0;
    let max = 0;
    for (let r = 0; r < arr.length; r++) {
        const item = arr[r];
        if (!count[item]) {
            count[item] = 0;
        }
        count[item]++;
        max = Math.max(max, count[item]);
        while (r - l + 1 - max > k) {
            count[arr[l]]--;
            l += 1;
        }
        res = Math.max(res, r - l + 1);
    }
    return res;
}


const res = charactersReplacement("ABAB");
console.log(res);
