/*
 * Complete the 'alternatingCharacters' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 * See description: https://www.hackerrank.com/challenges/alternating-characters/problem?isFullScreen=true&h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings
 */


function alternatingCharacters(s) {
    const str = s.split("");
    let cnt = 0;
    let prev = str[0];
    for (let i = 1; i < str.length; i++) {
        if (prev === str[i]) {
            cnt++
        } else {
            prev = str[i];
        }
    }

    return cnt;
}

const tests = ["AAAA", "BBBBB", "ABABABAB", "BABABA", "AAABBB"];

for (let test of tests) {
    console.log(alternatingCharacters(test));
}