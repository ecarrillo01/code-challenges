const assert = require('node:assert').strict;

// https://www.hackerrank.com/challenges/migratory-birds/problem
function migratoryBirds(arr) {
    let m = new Map();
    for (let item of arr) {
        if (m.get(item)) {
            m.set(item, m.get(item) + 1)
        } else {
            m.set(item, 1);
        }
    }
    let ret = m.keys().next().value;
    for (let [k, v] of m) {
        if (m.get(ret) < v) {
            ret = k;
        }
        else if (m.get(ret) === v) {
            if (ret > k) {
                ret = k;
            }
        }

    }

    return ret;
}

// tests
assert.equal(migratoryBirds([1, 4, 4, 4, 5, 3]), 4);
assert.equal(migratoryBirds([1, 1, 2, 2, 3]), 1);
assert.equal(migratoryBirds([1, 2, 3, 4, 5, 4, 3, 2, 1, 3, 4]), 3);
