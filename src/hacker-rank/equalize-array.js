//https://www.hackerrank.com/challenges/equality-in-a-array/problem
function equalizeArray(arr) {
    let frequencies = new Map();
    let maxFreqIndex = arr[0];
    for (let item of arr) {
        if (frequencies.has(item)) {
            frequencies.set(item, frequencies.get(item) + 1);
        } else {
            frequencies.set(item, 1);
        }

        if (frequencies.get(item) > frequencies.get(maxFreqIndex)) {
            maxFreqIndex = item;
        }
    }

    return arr.length - frequencies.get(maxFreqIndex);
}

{
    //console.log(equalizeArray([3, 3, 2, 1, 3]));
    // console.log(equalizeArray([1, 2, 3, 1, 2, 3, 3, 3]));
    console.log(equalizeArray([24, 29, 70, 43, 12, 27, 29, 24, 41, 12, 41, 43, 24, 70, 24, 100, 41, 43, 43, 100, 29, 70, 100, 43, 41, 27, 70, 70, 59, 41, 24, 24, 29, 43, 24, 27, 70, 24, 27, 70, 24, 70, 27, 24, 43, 27, 100, 41, 12, 70, 43, 70, 62, 12, 59, 29, 62, 41, 100, 43, 43, 59, 59, 70, 12, 27, 43, 43, 27, 27, 27, 24, 43, 43, 62, 43, 70, 29]));
}