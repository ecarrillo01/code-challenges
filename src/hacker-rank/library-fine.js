//https://www.hackerrank.com/challenges/library-fine/problem
function libraryFine(d1, m1, y1, d2, m2, y2) {
    const sameYear = y2 === y1;
    const sameYearAndMonth = sameYear && m2 === m1;
    if (sameYearAndMonth && d2 < d1) {
        return 15 * (d1 - d2);
    }

    if (m2 < m1 && sameYear) {
        return 500 * (m1 - m2);
    }

    if (y2 < y1) {
        return 10000;
    }
    return 0;
}

console.log(libraryFine(9, 6, 2015, 6, 6, 2015));
