//https://www.hackerrank.com/challenges/cut-the-sticks/problem
function cutTheSticks(arr) {
    const hasSameLength = () => {
        let firstItem = arr[0];
        if (arr.length === 1) return true;
        for (let i = 1; i < arr.length; i++) {
            if (arr[i] != firstItem) return false;
        }
        return true;
    }

    const r = [arr.length];

    while (!hasSameLength()) {
        let min = Math.min(...arr);
        arr = arr.reduce((acc, curr, i) => {
            return curr > min ? [...acc, curr - min] : acc;
        }, []);
        r.push(arr.length);
    }
    return r;
}

//  console.log(cutTheSticks([5, 4, 4, 2, 2, 8]));
console.log(cutTheSticks([1, 2, 3, 4, 3, 3, 2, 1]));