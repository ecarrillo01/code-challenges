//Original binary search method 
//https://medium.com/@jeffrey.allen.lewis/javascript-algorithms-explained-binary-search-25064b896470
//Binary search method adapted with following tutorial
//https://www.youtube.com/watch?v=CAyXHTqBIBU&list=PLSIpQf0NbcCltzNFrOJkQ4J4AAjW3TSmA
const binarySearchBetween = (array, target) => {
    let startIndex = 0;
    let endIndex = array.length - 1;
    while (startIndex <= endIndex) {
        let middleIndex = Math.floor((startIndex + endIndex) / 2);
        if (target === array[middleIndex]) {
            return middleIndex;
        }
        else if (target > array[middleIndex] && target < array[middleIndex - 1]) {
            return middleIndex;
        }

        else if (target < array[middleIndex] && target >= array[middleIndex + 1]) {
            return middleIndex + 1;
        }

        else if (target > array[middleIndex]) {
            endIndex = middleIndex - 1;

        }
        else if (target < array[middleIndex]) {
            startIndex = middleIndex + 1;
        }

    }
    return -1;

}

function climbingLeaderBoard(scores, alice) {
    const uniqueScores = Array.from(new Set(scores));
    let retArr = [];
    const uniqueScoresLength = uniqueScores.length;
    const aliceLength = alice.length;
    for (let i = 0; i < aliceLength; i++) {
        const aliceItem = alice[i];
        let alicePosition = -1;
        if (aliceItem < uniqueScores[uniqueScoresLength - 1]) {
            alicePosition = uniqueScoresLength + 1;
        }
        else if (aliceItem === uniqueScores[uniqueScoresLength - 1]) {
            alicePosition = uniqueScoresLength;
        }
        else if (aliceItem >= uniqueScores[0]) {
            alicePosition = 1;
        }

        else {
            alicePosition = 1 + binarySearchBetween(uniqueScores, aliceItem);
        }
        retArr.push(alicePosition);
    }

    return retArr;
}


console.log(climbingLeaderBoard([100, 90, 90, 80, 75, 60], [50, 65, 77, 90, 102]));