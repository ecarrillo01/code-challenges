// https://www.hackerrank.com/challenges/jumping-on-the-clouds/problem
function jumpingOnClouds(c) {
    let jumps = 0;
    let skipIndex = -1;
    if (c.length === 2) return 1;

    for (let i = 0; i < c.length; i++) {
        if (c[i] === 0 && skipIndex != i) {
            if (i === c.length - 2) {
                jumps++;
                break;
            }
            if (c[i + 2] === 0) {
                jumps++;
                skipIndex = i + 1;
            }
            if (c[i + 2] === 1) {
                jumps++;
            }
        }
    }
    return jumps;
}
let arr = [0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0];
console.log(jumpingOnClouds(arr));
console.log(jumpingOnClouds([0, 0, 1, 0, 0, 1, 0]));