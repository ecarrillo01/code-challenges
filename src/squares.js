//https://www.hackerrank.com/challenges/sherlock-and-squares/problem
// Suppose you want to calculate all the square integers between 3 and 14. calculate the square roots of the end points..that will be around 1.73 and 3.74. 
// Now the integers between 1.73 and 3.74 are 2 and 3 which is what we want.
// To get this we use the ceil function on 1.73 which becomes 2   and we use the floor function on 3.74 which becomes 3.
// Their difference is 1.
// We add 1 to the difference because we rounded off 1.73 to 2 and since 2 is an integer we need to consider it also.
function squares(a, b) {
    let ax = Math.ceil(Math.sqrt(a));
    let bx = Math.floor(Math.sqrt(b));
    let cnt = bx - ax + 1
    return cnt;
}