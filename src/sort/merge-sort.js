// Merge two sorted arrays
function merge(arr1, arr2) {
    let results = [];
    // Create 2 pointers
    let i = 0, j = 0;
    while (i < arr1.length && j < arr2.length) {
        if (arr1[i] < arr2[j]) {
            results.push(arr1[i]);
            i++;
        } else {
            results.push(arr2[j]);
            j++
        }
    }

    while (i < arr1.length) {
        results.push(arr1[i]);
        i++
    }

    while (j < arr2.length) {
        results.push(arr2[j]);
        j++
    }

    return results;
}

function mergeSort(arr) {
    if (arr.length <= 1) return arr;
    const mid = Math.floor(arr.length / 2);
    const left = mergeSort(arr.slice(0, mid));
    const right = mergeSort(arr.slice(mid));
    return merge(left, right);
}

// console.log(merge([1, 10, 50, 79], [2, 14, 99, 100]));
console.log(mergeSort([20, 1, 12, 8, 6, 33, 22, 13, 90, 50, 40, 38, 99]));
console.log(mergeSort([10, 24, 76, 73, 72, 1, 9]));