//https://www.geeksforgeeks.org/subset-no-pair-sum-divisible-k/
function nonDivisibleSubset(k, s) {
    let f = new Array(k).fill(0);
    for (let value of s) {
        f[value % k]++;
    }

    if (k % 2 == 0) {
        f[k / 2] = Math.min(f[k / 2], 1);
    }

    let res = Math.min(f[0], 1);
    for (let i = 1; i <= Math.floor(k / 2); i++) {
        res += Math.max(f[i], f[k - i]);
    }

    return res;
}


const arr = [278, 576, 496, 727, 410, 124, 338, 149, 209, 702, 282, 718, 771, 575, 436];
//const arr2 = [3, 7, 2, 9, 1];
console.log(nonDivisibleSubset(7, arr));
// console.log(nonDivisibleSubset(3, arr2));
