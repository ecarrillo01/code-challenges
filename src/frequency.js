function Solution(S) {
    const lettersFreq = {};
    var result = " ";
    const strArr = S.split("");
    for (let key of strArr) {
        lettersFreq[key] = ++lettersFreq[key] || 1;
    }

    const arr = Object.entries(lettersFreq).sort(([k1, v1], [k2, v2]) => {
        if (v1 > v2) return -1
        if (v1 < v2) return 1;
        if (k1 < k2) return -1;
        return 1
    });
    for (let [k] of arr) {
        result += k;
    }

    return result;
}

console.log(Solution('llljkgghi'));