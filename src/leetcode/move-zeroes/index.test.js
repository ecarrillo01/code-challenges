const moveZeroes = require('./index');

describe('Move zeroes test suite', () => {
    it.each([
        {
            input: [0, 1, 0, 3, 12], expected: [1, 3, 12, 0, 0],
        },
        {
            input: [0], expected: [0],
        },
        {
            input: [0, 0], expected: [0, 0]
        },
        {
            input: [0, 0, 1], expected: [1, 0, 0]
        }
    ])('Move zeroes %s', ({ input, expected }) => {
        expect(moveZeroes(input)).toEqual(expected);
    });
});



