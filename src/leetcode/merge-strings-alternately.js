/**
 * https://leetcode.com/problems/merge-strings-alternately/?envType=study-plan-v2&envId=leetcode-75
 * You are given two strings word1 and word2. Merge the strings by adding letters in alternating order, starting with word1.
 * If a string is longer than the other, append the additional letters onto the end of the merged string.
 * @param {string} word1
 * @param {string} word2
 * @return {string}
 */
const mergeAlternately = function (word1, word2) {
    const max = Math.max(word1.length, word2.length);
    let retVal = '';
    for (let i = 0; i < max; i++) {
        if (word1[i] && word2[i]) {
            retVal += word1[i] + word2[i];
        }
        else if (word1[i]) {
            retVal += word1[i];
        }
        else if (word2[i]) {
            retVal += word2[i];
        }
    }
    return retVal;
};