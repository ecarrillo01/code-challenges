/**
 * https://leetcode.com/problems/string-compression/?envType=study-plan-v2&envId=leetcode-75
 * @param {character[]} chars
 * @return {number}
 */
const compress = function (chars) {
    const max = chars.length;
    let i = 0;
    let res = 0;
    if (chars.length === 1) return chars.length;
    while (i < max) {
        let group = 1;
        while (chars[i] === chars[i + group] && i + group < max) {
            group++;
        }
        chars[res] = chars[i];
        res++;
        if (group > 1) {
            const groupStr = group.toString();
            for (let j = 0; j < groupStr.length; j++) {
                chars[res] = groupStr[j];
                res++;
            }
        }
        i += group;
    }
    return res;
}

module.exports = compress;