const reverseWords = require('./index');
describe('Reverse vowels of a string unit tests', () => {
    it.each([
        { a: 'the sky is blue', expected: 'blue is sky the' },
        {
            a: '  hello world  ', expected: 'world hello'
        },
    ])('Reverse words in the string $a should be: $expected', ({ a, expected }) => expect(reverseWords(a)).toBe(expected));

})