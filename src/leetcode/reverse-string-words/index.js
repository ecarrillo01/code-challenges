/**
 * https://leetcode.com/problems/reverse-words-in-a-string/description/?envType=study-plan-v2&envId=leetcode-75
 * @param {string} s
 * @return {string}
 */
const reverseWords = function (s) {
    return s.trim().split(/\s+/).reverse().join(' ');
};

module.exports = reverseWords;