/**
 * https://leetcode.com/problems/container-with-most-water/?envType=study-plan-v2&envId=leetcode-75
 * @param {number[]} height
 * @return {number}
 */
const maxArea = function (height) {
    let max = 0;
    let left = 0;
    let right = height.length - 1;
    while (left < right) {
        const currentArea = (right - left) * Math.min(height[left], height[right]);
        if (height[left] < height[right]) {
            left++;
        } else {
            right--;
        }
        max = Math.max(currentArea, max);
    }
    return max;
};

console.log(maxArea([1, 8, 6, 2, 5, 4, 8, 3, 7]));

module.exports = maxArea;