/**
 * leetcode 1071
 * https://leetcode.com/problems/greatest-common-divisor-of-strings/?envType=study-plan-v2&envId=leetcode-75
 * @param {string} str
 * @param {string} str2
 * @return {string}
 */

const isDivisor = (str, chunk) => {
    if (str.startsWith(chunk)) {
        const times = Math.floor(str.length / chunk.length);
        if (chunk.repeat(times) === str) return true;
    }
    return false;
}


const gcdOfStrings = function (str1, str2) {
    const minLen = Math.min(str1.length, str2.length);
    if (str1 + str2 !== str2 + str1) return '';
    for (let i = minLen; i > 0; i--) {
        const chunk = str1.substr(0, i)
        if (isDivisor(str1, chunk) && isDivisor(str2, chunk)) return chunk;
    }
    return '';
};


console.log(gcdOfStrings('ABC', 'ABCABC'))