const increasingTriplet = require('./index');

describe('Increasing triplet', () => {
    it.each([
        [[1, 2, 3, 4, 5], true],
        [[5, 4, 3, 2, 1], false],
        [[2, 1, 5, 0, 4, 6], true],
        [[20, 100, 10, 12, 5, 13], true],
    ])('Increasing triplet %s', (arr, expected) => {
        expect(increasingTriplet(arr)).toBe(expected)
    });
});



