const productExceptSelf = require('./index');

const divisionSolution = (arr) => {
    let totalProduct = arr.reduce((prev, curr) => prev * curr);
    return arr.map(item => totalProduct / item);
}

describe('Product except itself', () => {
    it.each([
        { arr: [4, 5, 1, 8, 2, 10, 6] },
        { arr: [1, 2, 3, 4] },
    ])('Product except itself test', ({ arr }) => {
        const actual = productExceptSelf(arr);
        expect(actual).toEqual(divisionSolution(arr));
    });
});



