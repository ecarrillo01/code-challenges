function elevatorTest(arr, maxPeople, maxWeight) {
    let stops = 0;
    let left = 0;
    let right = left + maxPeople;;
    while (left < arr.length) {
        stops++;
        let sum = 0;
        right = left + maxPeople - 1;
        for (let i = left; i <= right; i++) {
            if (i >= arr.length) {
                left = arr.length;
                break;
            }
            sum += arr[i].weight;
            if (sum > maxWeight) {
                left = i;
                break;
            }
            if (i === right) {
                left += maxPeople;
            }
        }
    }
    return stops;
}




const line1 = [
    { weight: 150, floor: 2, }, { weight: 200, floor: 3, },
    { weight: 120, floor: 5, }, { weight: 80, floor: 2, },
    { weight: 180, floor: 4, }, { weight: 170, floor: 4, }];
console.log(elevatorTest(line1, 2, 340));
