const alpha = Array.from(Array(26)).map((_, i) => i + 65);
const alphabet = alpha.map((x) => String.fromCharCode(x));
console.log(alphabet);

function toAlphabet(a = 'A', b = 'Z') {
    const arr = [];
    let i = a.charCodeAt(0);
    const j = b.charCodeAt(0);
    for (; i <= j; ++i) {
        console.log(i);
        arr.push(String.fromCharCode(i));
    }
    return arr;
}

console.log(toAlphabet())