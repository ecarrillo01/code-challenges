const isSubsequence = require('./isSubsequence');

describe('isSubsequence test suite', () => {
    it.each([
        { a: 'hello', b: 'hello world', expected: true },
        { a: 'sing', b: 'sting', expected: true },
        { a: 'abc', b: 'abracadabra', expected: true },
        { a: 'abc', b: 'acb', expected: false },
    ])('isSubsequence($a,$b) should be $expected', ({ a, b, expected }) => expect(isSubsequence(a, b)).toBe(expected));
});