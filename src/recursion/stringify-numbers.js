// https://www.udemy.com/course/js-algorithms-and-data-structures-masterclass/learn/quiz/425988#overview

// Write a function called stringifyNumbers which takes in an object and finds all of the values which are numbers and converts them to strings. Recursion would be a great way to solve this!

// The exercise intends for you to create a new object with the numbers converted to strings, and not modify the original. Keep the original object unchanged.

function stringifyNumbers(obj) {
    const newOb = {};
    for (let [k, item] of Object.entries(obj)) {
        if (typeof item === 'number') {
            newOb[k] = item.toString();
        }
        else if (item !== undefined && item !== null && item.constructor == Object) {
            newOb[k] = stringifyNumbers(item);
        }
        else {
            newOb[k] = item;
        }

    }
    return newOb;
}


let obj = {
    num: 1,
    test: [],
    data: {
        val: 4,
        info: {
            isRight: true,
            random: 66
        }
    }
}


stringifyNumbers(obj)

/*
{
    num: "1",
    test: [],
    data: {
        val: "4",
        info: {
            isRight: true,
            random: "66"
        }
    }
}
*/