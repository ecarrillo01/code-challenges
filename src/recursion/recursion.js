function countdown(n) {
    if (!n) return [];
    return [n].concat(countdown(n - 1));
}

function decimalToBinary(n) {
    if (!n) return '';
    return decimalToBinary(Math.floor(n / 2)) + (n % 2);
}

function recursiveSummation(n) {
    if (n <= 1) return n;
    return n + recursiveSummation(n - 1)
}

console.log(countdown(10));
//console.log(decimalToBinary(153));