const flattenArray = (arr) => arr.reduce(function (flattened, item) {
    if (Array.isArray(item)) {
        return flattened.concat(flattenArray(item));
    }
    return flattened.concat(item);
}, []);


const arr = [1, [2, [3, [4]], 5]];
console.log(flattenArray(arr));
//console.log(arr.flat(Infinity))
//console.log(arr.join().split(','))