/**
 * Taken from: https://practice.geeksforgeeks.org/problems/subarray-with-given-sum-1587115621/1?utm_source=geeksforgeeks&utm_medium=article_practice_tab&utm_campaign=article_practice_tab
 * Given an unsorted array A of size N that contains only positive integers, find a continuous sub-array that 
 * adds to a given number S and return the left and right index(1-based indexing) of that subarray.
 * In case of multiple subarrays, return the subarray indexes which come first on moving from left to right.
 * Note:- You have to return an ArrayList consisting of two elements left and right. In case no such subarray exists return an array consisting of element -1.
 * Example 1:
 * Input:
 * N = 5, S = 12
 * A[] = {1,2,3,7,5}
 * Output: 2 4
 * Explanation: The sum of elements 
 * from 2nd position to 4th position 
 * is 12.
 * 
 * Example 2:
 * Input:
 * N = 10, S = 15
 * A[] = {1,2,3,4,5,6,7,8,9,10}
 * Output: 1 5
 * Explanation: The sum of elements 
 * from 1st position to 5th position
 * is 15.
**/

function subArraySum(arr, s) {
    if (s === 0) return [-1];
    for (let left = 0, sum = 0, right = 0; right < arr.length; right++) {
        sum += arr[right];
        if (sum > s) {
            while (sum > s) {
                sum -= arr[left];
                left++;
            }
        }
        if (sum === s) {
            return [left + 1, right + 1];
        }
    }

    return [-1];
}

console.log(subArraySum([1, 2, 3, 7, 5], 12));
console.log(subArraySum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 15));
console.log(subArraySum([1, 4, 20, 3, 10, 5], 33));