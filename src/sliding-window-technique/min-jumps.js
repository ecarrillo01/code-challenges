//https://www.geeksforgeeks.org/minimum-number-of-jumps-to-reach-end-of-a-given-array/
function minJumps(arr) {
    const n = arr.length;
    if (n <= 1) // If there is only one element or the array is empty, we have reached the end of the array with 0 jumps
        return 0;
    if (arr[0] === 0) // If the first element is 0, we can't move forward
        return -1;

    let maxReach = arr[0]; // Stores the maximum index that can be reached with the current jump
    let steps = arr[0]; // Stores the number of steps that can be taken from the current index
    let jumps = 1; // Stores the number of jumps taken so far

    for (let i = 1; i < n; i++) {
        if (i === n - 1) // If we have reached the end of the array, return the number of jumps taken so far
            return jumps;
        maxReach = Math.max(maxReach, i + arr[i]); // Update the maximum index that can be reached with the current jump
        steps--; // Decrement the number of steps that can be taken from the current index
        if (steps === 0) { // If no more steps can be taken from the current index, we need to take another jump
            jumps++; // Increment the number of jumps taken so far
            if (i >= maxReach) // If the current index is greater than the maximum index that can be reached, return -1
                return -1;
            steps = maxReach - i; // Update the number of steps that can be taken from the current index
        }
    }
    return -1; // If we haven't reached the end of the array, return -1
}


const testSuit = [
    [2, 3, 1, 1, 2, 4, 2, 0, 1, 1],//4
    [
        33, 76, 21, 77, 100, 68, 37, 8, 22, 69, 81, 38,
        94, 57, 76, 54, 65, 14, 89, 69, 4, 16, 24, 47,
        7, 21, 78, 53, 17, 81, 39, 50, 22, 60, 93, 89,
        94, 30, 97, 16, 65, 43, 20, 24, 67, 62, 78, 98,
        42, 67, 32, 46, 49, 57, 60, 56, 44, 37, 75, 62,
        17, 13, 11, 40, 40, 4, 95, 100, 0, 57, 82, 31,
        0, 1, 56, 67, 30, 100, 64, 72, 66, 63, 18, 81,
        19
    ],//2
    [
        1, 3, 5, 8, 9,
        2, 6, 7, 6, 8,
        9
    ],//3
    [
        9, 10, 1, 2, 3, 4,
        8, 0, 0, 0, 0, 0,
        0, 0, 1
    ]//2
];

for (let i = 0; i < testSuit.length; i++) {
    console.log(minJumps(testSuit[i]));
}


