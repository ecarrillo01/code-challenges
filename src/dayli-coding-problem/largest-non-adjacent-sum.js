// Given a list of integers, write a function that returns the largest sum of non-adjacent numbers. Numbers can be 0 or negative.
// For example, [2, 4, 6, 2, 5] should return 13, since we pick 2, 6, and 5. [5, 1, 1, 5] should return 10, since we pick 5 and 5.
// Follow-up: Can you do this in O(N) time and constant space?

let ob =[];

//Solution 1, O(2^n)
function largestNonAdjecentSum1(arr, call) {
    //call && ob[call].push(arr);
    if (!arr || arr.length === 0) {
        return 0;
    }

    const x = largestNonAdjecentSum1(arr.slice(1), 'x');
    const y = arr[0] + largestNonAdjecentSum1(arr.slice(2), 'y');
    const max = Math.max(x, y);
    ob.push([x,y]);
    return max;
}

setTimeout(() => {
    console.log(ob);
}, 1000);

//Solution 2: O(n) and n space
function largestNonAdjecentSum2(arr) {
    if (arr.length <= 2) {
        return Math.max(0, Math.max(arr));
    }

    const cache = new Array(arr.length).fill(0);
    cache[0] = Math.max(0, arr[0]);
    cache[1] = Math.max(cache[0], arr[1]);

    for (let i = 2; i < arr.length; i++) {
        cache[i] = Math.max(arr[i] + cache[i - 2], cache[i - 1]);
    }

    return cache[arr.length - 1]
}

//Solution 3: O(n) and constant space
function largestNonAdjecentSum3(arr) {
    if (arr.length <= 2) {
        return Math.max(0, Math.max(arr));
    }

    let maxExcludingLast = Math.max(0, arr[0]);
    let maxIncludingLast = Math.max(maxExcludingLast, arr[1]);

    for (let i = 2; i < arr.length; i++) {
        //For storing temporarily
        const prevMaxIncludingLast = maxIncludingLast;

        maxIncludingLast = Math.max(maxIncludingLast, arr[i] + maxExcludingLast);
        maxExcludingLast = prevMaxIncludingLast;
    }

    return Math.max(maxExcludingLast, maxIncludingLast);
}

const arrays = [
    // [2, 4, 6, 2, 5],  //2 + 6 + 5 = 13
    [5, 1, 2, 5], //5 + 5 = 10
    // [5, 1, 5, 1, 1, 9, 1], // 5 + 5 + 9 = 19
];
console.log('Solution 1');
arrays.forEach((arr) => console.log(largestNonAdjecentSum1(arr)));
// console.log('Solution 2');
// arrays.forEach((arr) => console.log(largestNonAdjecentSum2(arr)));
// console.log('Solution 3');
// arrays.forEach((arr) => console.log(largestNonAdjecentSum3(arr)));