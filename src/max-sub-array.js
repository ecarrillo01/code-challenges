function maxSubArray(arr) {
    let maxSum = arr[0];
    let currentSum = 0;
    for (let i = 0, max = arr.length; i < max; i++) {
        currentSum = Math.max(arr[i], currentSum + arr[i]);
        //compare maxSum with currentSum and store the greater value
        maxSum = Math.max(currentSum, maxSum);
    }
    return maxSum;
}


console.log(maxSubArray([-2, -3, 4,- 1, -2, 1, 5, -3]));